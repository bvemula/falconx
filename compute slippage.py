from websocketSlipageCalculator import WebsocketSlipageCalculator

try:
    # wrapper around cbpro library
    # controls socker connection and computes slippage
    wsClient = WebsocketSlipageCalculator(url="wss://ws-feed.pro.coinbase.com", products="BTC-USD",
    should_print=False, channels= ["level2","matches"])

    # start the slippag executioner
    wsClient.start()
except:
    print('An Error occured, but execution is continued hoping good thing in the future stream!!')
