import os

INIT_DELAY=os.environ.get('AUTO_CLEAN_DB')
if INIT_DELAY is None:
    INIT_DELAY = 5

INTERVAL=os.environ.get('AUTO_CLEAN_DB')
if INTERVAL is None:
    INTERVAL = 1
