import cbpro
import json
import datetime
from collections import OrderedDict
from config import INTERVAL, INIT_DELAY
import threading

# function that executes every k seconds
def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()
    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t

class WebsocketSlipageCalculator(cbpro.WebsocketClient):
    # util function to calculate and print slippage
    def print_slippage(self, snapshot, matches, nextIndex):
        if len(matches) == 0:
            print('No macthes on', nextIndex,' slippage: 0')
            return
        matchSize = 0
        # calculate executed price
        executed_price = 0
        for match in matches:
            current_match_size = float(match['size'])
            matchSize += current_match_size
            executed_price += (float(match['price']) * current_match_size)
        executed_price = executed_price / matchSize
        # calculate expected price
        values = []
        for key in snapshot:
            price = float(key)
            size = float(self.snapshot[key])
            if size > 0:
                values.append([ price, size , price / size ])
        values = sorted(values, key=lambda x: x[2])
        expected_price = 0
        left_match_size = matchSize
        for value in values:
            if left_match_size >= value[0]:
                left_match_size -= value[0]
                expected_price += (value[0] * value[1])
                if left_match_size == 0:
                    break
            else:
                expected_price += (value[1] * left_match_size)
                break
        expected_price = expected_price / matchSize
        slippage = ((executed_price - expected_price) * 100) / expected_price
        print('slippage on', nextIndex,' slippage: ', slippage)

    # util function to make updates in existing snapshot
    def updateSnapshot(self, updates):
        for update in updates:
            if update[1] == '0' and update[0] in self.snapshot:
                del self.snapshot[update[0]]
            else:
                self.snapshot[update[0]] = float(update[1])

    # util function to calculate Slippage
    def calc_slippage(self):
        if self.count < INIT_DELAY:
            self.count += 1
            return

        t_minus_start = datetime.datetime.utcnow() - datetime.timedelta(seconds=INIT_DELAY)
        nextIndex = t_minus_start.strftime("%Y-%m-%dT%H:%M:%S")

        matches = []
        updates = []
        # gather all the matches and updates for last k seconds
        for second in range(INTERVAL):
            sellChanges = []
            t_minus_start = datetime.datetime.utcnow() - datetime.timedelta(seconds=INIT_DELAY + second)
            nextIndex = t_minus_start.strftime("%Y-%m-%dT%H:%M:%S")
            if nextIndex in self.matchesBuffer:
                matches.extend(self.matchesBuffer[nextIndex])
                del self.matchesBuffer[nextIndex]
            if nextIndex in self.updatesBuffer:
                changes = self.updatesBuffer[nextIndex]
                for data in changes:
                    for change in data['changes']:
                        if change[0] == 'sell':
                            updates.append([change[1], float(change[2])])
                del self.updatesBuffer[nextIndex]
            snapshotCopy = self.snapshot.copy()
            self.updateSnapshot(updates) # propgate the updates to the original snapshot t_0 + 1
            self.print_slippage(snapshotCopy, matches, nextIndex) # calculate slippage for t_0


    # wrapper function which is called on recieving a message from feed
    def on_message(self, message):
        if message['type'] == 'snapshot':
            self.snapshot = {} # maintain the current snapshot
            self.updatesBuffer = {} # dictionary that maintains sell updates in a  buffer
            self.matchesBuffer = {} # dictionary that maintains matches update in a buffer
            self.count = 0
            for bid in message['bids']:
                self.snapshot[bid[0]] = float(bid[1])
            set_interval(self.calc_slippage, INTERVAL)
        else:
            # buffer match updates
            if message['type'] == 'match':
                index = message['time'].split('.')[0]
                if index not in self.matchesBuffer:
                    self.matchesBuffer[index] = []
                self.matchesBuffer[index].append(message)
            # buffer l2 updates
            elif message['type'] == 'l2update':
                index = message['time'].split('.')[0]
                if index not in self.updatesBuffer:
                    self.updatesBuffer[index] = []
                self.updatesBuffer[index].append(message)
                # print(index)
