
# slippage calculator

The following program calculates slippage for crypto currencies
Problem statement can be found in the following link
https://docs.google.com/document/d/1BPf6zjjT0iN3znL4V2QCqfFXKwb2I_zzjElFPbHRLkM/edit

## requirements

python 3

## Installing Dependencies

pip3 -r requirements.txt

## running the program

sudo python3 compute\ slippage.py


## configurations

Edit config.py file or set the following environment variables to modify the behaviour of execution

INIT_DELAY = x  ( calculate slippage at time currentTime - x seconds )
INTERVAL = y ( calculate the slippage for y seconds)

## output format

No macthes on 2019-01-15T14:00:16  slippage: 0
slippage on 2019-01-15T14:00:17  slippage:  -95.47198049695007
slippage on 2019-01-15T14:00:18  slippage:  -95.53439597356052


## slippage calculation Refernces

http://www.tradingblox.com/Manuals/UsersGuideHTML/slippage_percentage.htm
